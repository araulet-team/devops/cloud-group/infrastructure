# Gitlab Pipeline [![pipeline status](https://gitlab.com/araulet-team/devops/cloud-group/infrastructure/badges/master/pipeline.svg)](https://gitlab.com/araulet-team/devops/gitlab-ci-group/release-pipeline/commits/master) [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Pipeline to build/deploy infrastructure with Terraform. on Digitalocean.

Droplet default settings (see [#advanced-configuration](#advanced-configuration)):
* size: s-1vcpu-1gb (5$ a month)
* image: docker
* region: us east

## Pre-requisites

#### Digitalocean

* Open an account on [Digitalocean](https://www.digitalocean.com)
* Create a [Access Token](https://cloud.digitalocean.com/account/api/tokens) needed to make operation on DigitalOcean API
* Create [Spaces Access Keys](https://cloud.digitalocean.com/account/api/tokens) needed to access Spaces API
* Create a bucket on [Spaces](https://cloud.digitalocean.com/spaces) (S3 a like) this operation can't be automated at the moment.
    * Click on `Create` button on top and select `Spaces`

## Usage

Gitlab-ci will do all the work to create your Droplet as long you **have gone through the pre-requisites** :)

#### Terraform states

There are 2 terraform states:
* **global** resources (tags, projects, ssh keys) meant to be resused by other terraform resouces like droplet. These resources needs to be available accross all branches. All branches are going to target the same terraform state.
* **droplets**: use to create infrastructure. 

Terraform states are uploaded in the bucket that you previsouly have created (see prerequisite)

e.g.

| branch | state file |
| ------ | ---------- |
| **master**                        | `master/terraform.tfstate` |
| **feature/digitalocean-provider** | `feature-digitalocean-provider/terraform.tfstate` |
| ---                               | `global/terraform.tfstate` |

#### features branches

1.  Droplet created on feature branches will be associated to a project called **playground** by default. 
2.  To not conflict if muliple developers are working on different branches, droplets are prefix with `CI_COMMIT_REF_SLUG`.

#### master

1.  Droplets created master branch will be associated to a project called **production** by default.
2.  Droplets on master branch are prefix with `stable`

#### Droplets

You can spawn multiple droplets.
* **do_droplet_count**: number of droplets you want
* **do_droplet_images**: list of images to be applied
* **do_droplet_regions**: list of regions to be applied
* **do_droplet_sizes**: list of instance size to be applied
* **do_droplet_monitoring**: enabled/disabled monitoring

## Advanced Configuration 


#### Gitlab variables

**everything there is needed**

| name | default | required | note |
| ---- | ------- | -------- | ---- |
| **DIGITALOCEAN_TOKEN**                | `sensible` | X | See prerequisite (needed for DigitalOcean Terraform provider) |
| **DO_SPACES_ACCESS_KEY**              | `sensible` | X | See prerequisite |
| **DO_SPACES_ACCESS_SECRET**           | `sensible` | X | See prerequisite |
| **TF_VAR_do_space_tf_bucket**         | | X | Space created to store terraform state |
| **TF_VAR_do_prod_ssh_pub_key**     | `sensible` | X | root public key to ssh your prod Droplet ([generate ssh keys](https://help.github.com/en/enterprise/2.17/user/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)) |
| **TF_VAR_do_prod_ssh_pri_key**     | `sensible` | X | root private key to ssh your prod Droplet | 
| **TF_VAR_do_dev_ssh_pub_key**     | `sensible` | X | root public key to ssh your development Droplets ([generate ssh keys](https://help.github.com/en/enterprise/2.17/user/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key)) |
| **TF_VAR_do_dev_ssh_pri_key**     | `sensible` | X | root private key to ssh your development Droplets | 
| **USER_SSH_PUB_KEY**              | `sensible` | x | public key for user `sammy` created by Ansible |
| **USER_SSH_PRI_KEY**              | `sensible` | x | private key for user `sammy` created by Ansible |

#### Default Terraform variables

**Notes**: Terraform provide a mechanic to override variable through environment variable by prefixing them with `TF_VAR`.

##### `SSH Keys`

SSH keys created in DigitalOcean and used by Ansible in the CI to connect to the droplet.

* **master**: One key for your production cluster
* **branches**: another one used for all branches.

| name | default | note |
| ---- | ------- | ---- |
| **do_ssh_prod_key_name**   | `gitlabci-tf` | ssh key name (e.g **TF_VAR_do_prod_ssh_key_name** to override) |
| **do_prod_ssh_pub_key**    | `sensible` | provided by gitlab envs **TF_VAR_do_prod_ssh_pub_key** |
| **do_prod_ssh_pri_key**    | `sensible` | provided by gitlab envs **TF_VAR_do_prod_ssh_pri_key** |
| **do_ssh_dev_key_name**   | `gitlabci-tf-development` | ssh key name (e.g **TF_VAR_do_dev_ssh_key_name** to override) |
| **do_dev_ssh_pub_key**    | `sensible` | provided by gitlab envs **TF_VAR_do_dev_ssh_pub_key** |
| **do_dev_ssh_pri_key**    | `sensible` | provided by gitlab envs **TF_VAR_do_dev_ssh_pri_key** |

##### `Project Configuration`

There are 2 projects created by default

* **Production**: To host all resources related to production grade (master).
* **Playground**: To host all resources created in all branches except master.

| name | default | note |
| ---- | ------- | ---- |
| **do_project_count**      | `2` | project creation is enabled, set it to **0** if you prefer the Droplet to be created in your default project in DigitalOcean |
| **do_project_name**       | `["Production", "Playground"]` | Project name |
| **do_project_description**     | `["A project to represent production resources.", "A project to represent development resources."]` | Project description |
| **do_project_purpose**    | `["Web Application", "Web Application"]` | Project purpose |
| **do_project_environment**      | `["Production", "Development"]` | Project environment |

##### `Droplet Configuration`

Instances to be created in DigitalOcean. They will be tags "development" for branches and "production" for master.

| name | default | note |
| ---- | ------- | ---- |
| **do_droplet_count**      | `1` | number of droplet to spawn |
| **do_droplet_images**     | `[ "docker-18-04" ]` | list of images associated with each Droplets |
| **do_droplet_regions**    | `[ "nyc3" ]` | regions associated with each Droplets |
| **do_droplet_sizes**      | `[ "s-1vcpu-1gb" ]` | instance sizes associated with each Droplets |
| **do_droplet_monitoring** | `[ true ]` | enabled monitoring (be careful it's not supported for each images)
| **do_droplet_name_prefix** | `default` | prefix depends on the branch |


#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

if [[ "${DEBUG_INTERNAL:-}" == "true" ]]; then
  set -o xtrace
fi;

do_bearer_token="${DIGITALOCEAN_TOKEN}"

[[ "${CI_COMMIT_REF_SLUG:-}" == "master" ]] && {
	tag="production"
} || {
	tag="development"
}

droplets=$(curl -s -X GET \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${do_bearer_token}" \
"https://api.digitalocean.com/v2/droplets?tag_name=${tag}")

inventory=$(echo ${droplets} | jq '.droplets[] | [{name:.name,ansible_host:.networks.v4[].ip_address}]')

hosts=''
for obj in $(echo "${inventory}" | jq -r '.[] | @base64'); do
	decoded=$(echo $obj | base64 --decode)
	name=$(echo $decoded | jq -r '.name')
	ip=$(echo $decoded | jq -r '.ansible_host')
	hosts="${hosts}${name} ansible_host=${ip}\n"
done

echo -e "
	[$tag]
	$hosts
	[$tag:vars]
	ansible_python_interpreter=/usr/bin/python3
" > /tmp/inventory.${CI_JOB_ID}

cat /tmp/inventory.${CI_JOB_ID}



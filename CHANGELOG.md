# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.6.0
2020-03-03

### Features

- **ansible:** deploy nodejs boilerplate (55a10ad6611397c13d1eacd305aa2e88c57abd25)

## 0.5.3
2020-02-22

### Fixes

- improve README.md [skip-ci] (dd3b6e6a5d9932e1451215c79bc66cb51185b27e)

## 0.5.2
2020-02-19

### Fixes

- **terraform:** digitalocean_project ignore resources (31cc88b9af560ca6ec8c7525dda96b4f08b7e814)

## 0.5.1
2020-02-19

### Fixes

- **ci:** plan job (63adc6d870cf543c6fe4aa0eb35bc5d49ef2745a)

## 0.5.0
2020-02-19

### Features

- **project:** create new projects definition (a225a93b3c149e50b5310d93308fe53052adb3e6)

## 0.4.0
2020-01-08

### Features

- **ansible:** add user and ssh key (112009b170a51f8a072bd1921c6f4e20f72d5c27)

## 0.3.1
2019-12-15

### Fixes

- **ci:** ansible provisioning (private key/dynamic inventory) (b7f23696d2fd06f39f6559f761c55be0e2b60f74)

## 0.3.0
2019-12-14

### Features

- **ci:** split tf to create global and local resources (216492ba760a56bf521982214d778f323cc07797)
- **ansible:** allow provisioning with Ansible (1150d3fd5d95842f277f691dc1425633677aaf78)

### Fixes

- **cleanup:** remove terraform package (dafa4b46ed1b6f8acd2c1da671f8648778175fc7)
- **terraform:** Interpolation-only expressions are deprecated (52a22f162bdb3f7203ce5065896a6cfdddbcb37a)
- **terraform:** Quoted type constraints are deprecated (096aad5b1a809434d6e223c27cf80a434310c56b)
- **terraform:** ssh keys conflict (36bedf9368c4e0c82669b9dbbc809fda6ef24bea)
- **terraform:** Interpolation (a536c4f1db730643685a7d3e2c277111df76721b)
- **software:** fix Terraform and DO provider version (d09ef276878a6b7c295b3c7c45dcd498071e4e7e)
- **ci:** remove release job + disable it (549426277c65fe1ce49292615778644f57c542cf)
- **droplet:** ssh key id (687bf51fe74d353348a76f7274dd059344e4b7b7)

## 0.2.1
2019-10-28

### Fixes

- **ci:** prefix for droplet name from master (ace04410fcf32c8a0dab9a2384bdd9d2f7fb5696)

## 0.2.0
2019-10-28

### Features

- **terraform:** add DO provider (9bbb91e26f775297de82fd4fc24b72a8eb736268)

## 0.1.0
2019-10-26

### Features

- **project:** add default files (b8666ea0af21efad84e93aab82159b803902058d)


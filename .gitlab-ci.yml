---
variables:
  GIT_STRATEGY: clone
  GSG_RELEASE_BRANCHES: "master"
  RELEASE_DESC: "CI/CD framework for deploying infrastructure and containerized application."
  PLAN: plan.tfplan
  TF_VAR_CI_COMMIT_REF_SLUG: "${CI_COMMIT_REF_SLUG}"
  TIER_INFRASTRUCTURE: "global"

.job_definition: &job_definition
  image: docker:stable
  services:
    - docker:stable-dind
  variables:
    DOCKER_DRIVER: overlay2

stages:
  #
  # [infrastructure stages]
  #
  - validate
  - plan
  - apply
  - trigger
  - provision
  - prepare
  - release

.terraform_definition: &terraform_definition
  image:
    name: hashicorp/terraform:0.12.15
    entrypoint: [""]
  before_script:
    - |
      bucket_path="${CI_COMMIT_REF_SLUG}"
      if [[ "$TF_MODULE_PATH" == "do-global" ]]; then
        bucket_path="global"
      fi

      cd "./tiers/infrastructure/terraform/${TF_MODULE_PATH}"

      terraform --version

      AWS_PROFILE=digitalocean terraform init \
        -backend-config="bucket=${TF_VAR_DO_SPACE_TF_BUCKET}" \
        -backend-config="key=${bucket_path}/terraform.tfstate" \
        -backend-config="access_key=${TF_VAR_DO_SPACES_ACCESS_KEY}" \
        -backend-config="secret_key=${TF_VAR_DO_SPACES_ACCESS_SECRET}"
  only:
    refs:
      - branches

.validate_definition: &validate_definition
  <<: *terraform_definition
  stage: validate
  script:
    - terraform validate

.plan_definition: &plan_definition
  <<: *terraform_definition
  stage: plan
  artifacts:
    paths:
      - "./tiers/infrastructure/terraform/${TF_MODULE_PATH}/${CI_COMMIT_REF_SLUG}-${PLAN}"

.apply_definition: &apply_definition
  <<: *terraform_definition
  stage: apply
  script:
    - terraform apply -input=false "${CI_COMMIT_REF_SLUG}-${PLAN}"
    - terraform output -json
    - terraform show -json

.destroy_definition: &destroy_definition
  <<: *terraform_definition
  stage: apply
  when: manual
  script:
    - terraform destroy -auto-approve
    - terraform refresh

#
# [stage infrastructure - validate]
#
# - validate: syntax
#

tf:validate:global:
  <<: *validate_definition
  variables:
    TF_MODULE_PATH: "do-global"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "global"

tf:validate:droplet:
  <<: *validate_definition
  variables:
    TF_MODULE_PATH: "do-droplet"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"

#
# [stage infrastructure - plan]
#
# - plan: show what Terraform is going to apply
#   and save it in an artifact to be reused
#   by "apply" command
#

tf:plan:global:
  <<: *plan_definition
  variables:
    TF_MODULE_PATH: "do-global"
  script:
    - |
      terraform plan \
        -input=false \
        -out="${CI_COMMIT_REF_SLUG}-${PLAN}"
    - terraform show -no-color "${CI_COMMIT_REF_SLUG}-${PLAN}"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "global"

.plan_droplet_definition: &plan_droplet_definition
  script:
    - |
      terraform plan \
        -var "do_droplet_name_prefix=${do_droplet_name_prefix}" \
        -input=false \
        -out="${CI_COMMIT_REF_SLUG}-${PLAN}"
    - terraform show -no-color "${CI_COMMIT_REF_SLUG}-${PLAN}"

tf:plan:droplet:dev:
  <<: *plan_definition
  <<: *plan_droplet_definition
  variables:
    do_droplet_name_prefix: "${CI_COMMIT_REF_SLUG}"
    TF_MODULE_PATH: "do-droplet"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"
  except:
    refs:
      - master

tf:plan:droplet:master:
  <<: *plan_definition
  <<: *plan_droplet_definition
  variables:
    do_droplet_name_prefix: "stable"
    TF_MODULE_PATH: "do-droplet"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"
    refs:
      - master

#
# [stage infrastructure - apply]
#
# create resources
#

tf:apply:global:
  <<: *apply_definition
  variables:
    TF_MODULE_PATH: "do-global"
  after_script:
  - ./scripts/associate-project.sh "do:droplet:123123"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "global"

tf:apply:droplet:
  <<: *apply_definition
  variables:
    TF_MODULE_PATH: "do-droplet"
  after_script:
    - cd "./tiers/infrastructure/terraform/${TF_MODULE_PATH}"
    - apk add --update curl jq
    - droplet_id=$(terraform output -json droplet_id | jq -r '.[]')
    - echo $droplet_id
    - ./scripts/associate-project.sh "${droplet_id}"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"

#
# [stage infrastructure - destroy]
#

tf:destroy:global:
  <<: *destroy_definition
  variables:
    TF_MODULE_PATH: "do-global"
  only:
    refs:
      - branches
    variables:
      - $TIER_INFRASTRUCTURE == "global"

tf:destroy:droplet:
  <<: *destroy_definition
  variables:
    TF_MODULE_PATH: "do-droplet"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"

#
# [stage infrastructure - trigger]
#  terraform global trigger terraform droplet
#

trigger:
  stage: trigger
  script:
    - |
      curl --request POST \
      --form "token=${CI_JOB_TOKEN}" \
      --form ref=${CI_COMMIT_REF_NAME} \
      --form "variables[TIER_INFRASTRUCTURE]=droplet" \
      https://gitlab.com/api/v4/projects/15027924/trigger/pipeline
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "global"

#
# [stage infrastructure]
# - provision: ansible
#
.ssh_key_definition: &ssh_key_definition |
  touch /tmp/key.${CI_JOB_ID}
  chmod 600 /tmp/key.${CI_JOB_ID}

  [[ "${CI_COMMIT_REF_SLUG:-}" == "master" ]] && {
    echo "${TF_VAR_do_prod_ssh_pri_key}" > /tmp/key.${CI_JOB_ID}
  } || {
    echo "${TF_VAR_do_dev_ssh_pri_key}" > /tmp/key.${CI_JOB_ID}
  }

.inventory_definition: &inventory_definition |
  apt-get update -qq && apt-get install -y -qq curl jq
  ./scripts/inventory.sh

.ansible_definition: &ansible_definition
  image:
    name: williamyeh/ansible:ubuntu18.04
    entrypoint: [""]
  before_script:
    - *ssh_key_definition
    - *inventory_definition
    - ANSIBLE_CONFIG=./ansible.cfg ansible --version
    - ANSIBLE_CONFIG=./ansible.cfg ansible-playbook --version
    - cd ./tiers/infrastructure/ansible/
  after_script:
    - rm "/tmp/key.${CI_JOB_ID}" "/tmp/inventory.${CI_JOB_ID}"
  only:
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"

ansible:facts:
  <<: *ansible_definition
  stage: provision
  when: manual
  script:
    - |
      ANSIBLE_CONFIG=./ansible.cfg ansible all \
      --private-key="/tmp/key.${CI_JOB_ID}" \
      -u root \
      -i /tmp/inventory.${CI_JOB_ID} \
      -m ping
    - |
      ANSIBLE_CONFIG=./ansible.cfg ansible all \
      --private-key="/tmp/key.${CI_JOB_ID}" \
      -u root \
      -i /tmp/inventory.${CI_JOB_ID} \
      -m setup --tree /tmp/facts
  artifacts:
    paths:
      - /tmp/facts
    expire_in: 1m

ansible:provision:
  <<: *ansible_definition
  stage: provision
  script:
     - |
      ANSIBLE_CONFIG=./ansible.cfg ansible-playbook \
      --private-key="/tmp/key.${CI_JOB_ID}" \
      -u root \
      -i /tmp/inventory.${CI_JOB_ID} \
      main.yml

#
# [prepare stage]
#
# Create:
# - "build_info" file used for the release commited in the repository
# - ".next-version" file with the next version bump to be commited
# - "CHANGELOG" file if needed
#

prepare:
  stage: prepare
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  script:
    - release test-git || true
    - release test-api
    - release next-version > .next-version
    - release changelog || true
    - echo "RELEASE_DESC=\"${RELEASE_DESC}\"" >> build_info
    - echo "RELEASE_SHA=${CI_COMMIT_SHA}" >> build_info
    - echo "RELEASE_VERSION=$(<.next-version)" >> build_info
    - cat build_info
    - cat .next-version
  artifacts:
    paths:
      - build_info
      - .next-version
      - CHANGELOG.md
    expire_in: 1d
  only:
    refs:
      - master
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"
  except:
    - tags

#
# [release stage]
#
# master
# - create new release notes
# - commit changelog, release note
# - tagged the branch
#

.release_info: &release_info |
  # update release notes
  rm -f "${RELEASE_FILE}"
  mv build_info "${RELEASE_FILE}"
  cat "${RELEASE_FILE}"
  . "${RELEASE_FILE}"

.release_info_definition: &release_info_definition
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4
  stage: release
  when: on_success
  before_script:
    - *release_info
  only:
    refs:
      - master
    variables:
      - $TIER_INFRASTRUCTURE == "droplet"

release:
  <<: *release_info_definition
  dependencies:
    - prepare
  variables:
    RELEASE_FILE: release_info
  script: |
    release test-git && {
      echo "[YES] some changes have been detected."
      release commit-and-tag "${RELEASE_FILE}" CHANGELOG.md
      release --ci-commit-tag "v${RELEASE_VERSION}"
    } || {
      echo "[NO] no changes have been detected."
    }
# Digitalocean tag
variable "do_prod_tag" {
  type        = string
  description = "tag name for production droplet on digitalocean"
  default     = "production"
}

variable "do_dev_tag" {
  type        = string
  description = "tag name for development droplet on digitalocean"
  default     = "development"
}

# Digitalocean SSH resource Configuration
variable "do_prod_ssh_key_name" {
  type        = string
  description = "ssh key name for production droplet on digitalocean"
  default     = "gitlabci-tf"
}

variable "do_prod_ssh_pub_key" {
  type        = string
  description = "ssh public key for production droplet on digitalocean"
  default     = ""
}

variable "do_prod_ssh_pri_key" {
  type        = string
  description = "ssh private key for production droplet on digitalocean"
  default     = ""
}

variable "do_dev_ssh_key_name" {
  type        = string
  description = "ssh key name for development droplet on digitalocean"
  default     = "gitlabci-tf-development"
}

variable "do_dev_ssh_pub_key" {
  type        = string
  description = "ssh public key for development droplet on digitalocean"
  default     = ""
}

variable "do_dev_ssh_pri_key" {
  type        = string
  description = "ssh private key for development droplet on digitalocean"
  default     = ""
}

# Digitalocean Project resource
# https://www.terraform.io/docs/providers/do/r/project.html
variable "do_project_count" {
  type        = string
  description = "enabled/disabled the creation of the project"
  default     = 2
}

variable "do_project_name" {
  type        = list(string)
  description = "Project name"
  default     = [
    "Production",
    "Playground"
  ]
}

variable "do_project_description" {
  type        = list(string)
  description = "Project description"
  default     = [
    "A project to represent production resources.",
    "A project to represent development resources."
  ]
}

variable "do_project_purpose" {
  type        = list(string)
  description = "Project purpose"
  default     = [
    "Web Application",
    "Web Application"
  ]
}

# possible values: Development, Staging, Production
variable "do_project_environment" {
  type        = list(string)
  description = "Project environment"
  default     = [
    "Production",
    "Development"
  ]
}
# Configure DO remote backend for statefile
terraform {
  backend "s3" {
    endpoint                    = "nyc3.digitaloceanspaces.com"
    region                      = "us-east-1"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}

# Configure Digitalocean Provider
# ref: https://www.terraform.io/docs/providers/do/index.html
# pass through envrionment variable "DIGITALOCEAN_TOKEN"
provider "digitalocean" {
  version = "~> 1.11"
}

# Create tags
# ref: https://www.terraform.io/docs/providers/do/r/tag.html
resource "digitalocean_tag" "development" {
  name = var.do_dev_tag
}

resource "digitalocean_tag" "production" {
  name = var.do_prod_tag
}

# Configure ssh keys for production droplets
resource "digitalocean_ssh_key" "tfsshkey_prod" {
  name       = var.do_prod_ssh_key_name
  public_key = var.do_prod_ssh_pub_key
}

# Configure ssh keys for development droplets
resource "digitalocean_ssh_key" "tfsshkey_dev" {
  name       = var.do_dev_ssh_key_name
  public_key = var.do_dev_ssh_pub_key
}

# ref: https://www.terraform.io/docs/providers/do/r/project.html
resource "digitalocean_project" "definition" {
  count       = var.do_project_count
  name        = var.do_project_name[count.index]
  description = var.do_project_description[count.index]
  purpose     = var.do_project_purpose[count.index]
  environment = var.do_project_environment[count.index]

  lifecycle {
    ignore_changes = [
      resources,
    ]
  }
}
#
# SSH Resource outpout
# ref: https://www.terraform.io/docs/providers/do/d/ssh_key.html
#

output "ssh_id_prod" {
  value = "${digitalocean_ssh_key.tfsshkey_prod.id}"
}

output "ssh_name_prod" {
  value = "${digitalocean_ssh_key.tfsshkey_prod.name}"
}

output "ssh_fingerprint_prod" {
  value = "${digitalocean_ssh_key.tfsshkey_prod.fingerprint}"
}

output "ssh_id_dev" {
  value = "${digitalocean_ssh_key.tfsshkey_dev.id}"
}

output "ssh_name_dev" {
  value = "${digitalocean_ssh_key.tfsshkey_dev.name}"
}

output "ssh_fingerprint_dev" {
  value = "${digitalocean_ssh_key.tfsshkey_dev.fingerprint}"
}

#
# https://www.terraform.io/docs/providers/do/r/tag.html
#

output "tag_name_dev" {
  value = "${digitalocean_tag.development.name}"
}

output "tag_name_prod" {
  value = "${digitalocean_tag.production.name}"
}

#
# Project Resource outpout
# ref: https://www.terraform.io/docs/providers/do/r/project.html
#

output "project_id" {
  value = "${digitalocean_project.definition[*].id}"
}

output "project_owner_uuid" {
  value = "${digitalocean_project.definition[*].owner_uuid}"
}

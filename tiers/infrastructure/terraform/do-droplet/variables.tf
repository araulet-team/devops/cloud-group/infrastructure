# branches environement from which the pipeline is executed
variable "CI_COMMIT_REF_SLUG" {
  type        = string
  description = "branch name from gitlab"
  default     = ""
}

# remote terraform state
variable "DO_SPACE_TF_BUCKET" {
  type        = string
  description = "bucket for storing global state"
  default     = ""
}

variable "DO_SPACES_ACCESS_KEY" {
  type        = string
  description = "access key for digitalocean spaces"
  default     = ""
}

variable "DO_SPACES_ACCESS_SECRET" {
  type        = string
  description = "access token for digitalocean spaces"
  default     = ""
}

# Digitalocean SSH resource Configuration
variable "do_prod_ssh_key_name" {
  type        = string
  description = "ssh key name for production droplet on digitalocean"
  default     = "gitlabci-tf"
}

variable "do_dev_ssh_key_name" {
  type        = string
  description = "ssh key name for development droplet on digitalocean"
  default     = "gitlabci-tf-development"
}

# Digitalocean Droplet resource Configuration
variable "do_droplet_count" {
  type        = string
  description = "The number of instances to create"
  default     = 1
}

# curl -X GET --silent "https://api.digitalocean.com/v2/images?per_page=999" -H "Authorization: Bearer $TOKEN" |jq '.'
variable "do_droplet_images" {
  type    = list(string)
  default = [
    "ubuntu-18-04-x64"
  ]
}

variable "do_droplet_regions" {
  type        = list(string)
  description = "Droplet region"
  default     = [
    "nyc3"
  ]
}

variable "do_droplet_sizes" {
  type        = list(string)
  description = "Droplet size instance"
  default     = [
    "s-1vcpu-1gb"
  ]
}

variable "do_droplet_monitoring" {
  type    = list(bool)
  default = [
    true
  ]
}

variable "do_droplet_name_prefix" {
  type        = string
  description = "Droplet name prefix"
  default     = "default"
}


#
# Droplet Resource outpout
# ref: https://www.terraform.io/docs/providers/do/d/droplet.html

output "droplet_id" {
  value = "${digitalocean_droplet.web[*].id}"
}

output "droplet_image" {
  value = "${digitalocean_droplet.web[*].image}"
}

output "droplet_size" {
  value = "${digitalocean_droplet.web[*].size}"
}

output "droplet_vcpus" {
  value = "${digitalocean_droplet.web[*].vcpus}"
}

output "droplet_price_monthly" {
  value = "${digitalocean_droplet.web[*].price_monthly}"
}

output "droplet_ipv4_address" {
  value = "${digitalocean_droplet.web[*].ipv4_address}"
}

output "droplet_status" {
  value = "${digitalocean_droplet.web[*].status}"
}
# Configure DO remote backend for statefile
terraform {
  backend "s3" {
    endpoint                    = "nyc3.digitaloceanspaces.com"
    region                      = "us-east-1"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}

# Configure Digitalocean Provider
# ref: https://www.terraform.io/docs/providers/do/index.html
# pass through envrionment variable "DIGITALOCEAN_TOKEN"
provider "digitalocean" {
  version = "~> 1.11"
}

data "terraform_remote_state" "global" {
  backend = "s3"

  config = {
    endpoint                    = "nyc3.digitaloceanspaces.com"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    region                      = "us-east-1"
    bucket                      = var.DO_SPACE_TF_BUCKET
    key                         = "global/terraform.tfstate"
    access_key                  = var.DO_SPACES_ACCESS_KEY
    secret_key                  = var.DO_SPACES_ACCESS_SECRET
  }
}

# Configure Digitlaocean Droplet
# ref: https://developers.digitalocean.com/documentation/changelog/api-v2/new-size-slugs-for-droplet-plan-changes/
resource "digitalocean_droplet" "web" {
  count       = var.do_droplet_count
  image       = var.do_droplet_images[count.index]
  region      = var.do_droplet_regions[count.index]
  size        = var.do_droplet_sizes[count.index]
  name        = "${var.do_droplet_name_prefix}-${var.do_droplet_images[count.index]}-${count.index}"
  monitoring  = var.do_droplet_monitoring[count.index]

  tags        = [
    (var.CI_COMMIT_REF_SLUG == "master" ?
      data.terraform_remote_state.global.outputs.tag_name_prod :
      data.terraform_remote_state.global.outputs.tag_name_dev)
  ]

  ssh_keys    = [
    (var.CI_COMMIT_REF_SLUG == "master" ?
      data.terraform_remote_state.global.outputs.ssh_id_prod :
      data.terraform_remote_state.global.outputs.ssh_id_dev)
  ]
}
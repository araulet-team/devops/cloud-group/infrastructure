#!/usr/bin/env sh
set -o errexit
set -o pipefail
set -o nounset

# Associate a droplet to a specific project
# master is pushing on Production project
# others branches are pushing on deveopment project

if [[ "${DEBUG_INTERNAL:-}" == "true" ]]; then
  set -o xtrace
fi;

# do_bearer_token="${DIGITALOCEAN_TOKEN}"
do_bearer_token="${DIGITALOCEAN_TOKEN}"
droplet_urn=$1

[[ "${CI_COMMIT_REF_SLUG:-}" == "master" ]] && {
	PROJECT_ENV="Production"
} || {
	PROJECT_ENV="Development"
}

projects=$(curl -s -X GET \
	-H "Content-Type: application/json" \
	-H "Authorization: Bearer ${do_bearer_token}" \
	"https://api.digitalocean.com/v2/projects" )

project_id=$(echo "${projects}"  | jq -r '
	.projects[] |
	 select(.environment=='\"$PROJECT_ENV\"' and .purpose == "Web Application") |
	  .id')

echo "Adding \"${droplet_urn}\" to project id: ${project_id} for environment: ${PROJECT_ENV}"
echo '{"resources":["'"do:droplet:${droplet_urn}"'"]}'

curl -s -X POST \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${do_bearer_token}" \
-d '{"resources":["'"do:droplet:${droplet_urn}"'"]}' \
"https://api.digitalocean.com/v2/projects/${project_id}/resources" | jq .
